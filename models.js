var mongoose = require("mongoose");
var session = require('./models/session');
var item = require('./models/item');
var www = require('./bin/www');
var app = require('./app');

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true });

var db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function(callback){
});

var session = mongoose.model("session", session.sessionSchema);
var item = mongoose.model("item", item.itemSchema);

/**
 * All database operations functions
 */
function createItem(item) {
  var itm = new this.item({
    sessionId: item.sessionId,
    itemTitle: item.itemTitle,
    itemData: item.itemData,
    itemOwner: item.itemOwner
  });
  itm.save(function(error) {
    if (error) {
      console.error(error);
    } else {
      www.executeMessage({message:'notifyClients', value: item.sessionId});
    }
  });
}

function createSession(sessionId, sessionName, sessionPublic, sessionActive, sessionOwner) {
  var query = {sessionId: sessionId},
      update = {sessionName: sessionName, sessionPublic: sessionPublic, sessionActive: sessionActive, sessionOwner: sessionOwner},
      options = { upsert: true, new: true, setDefaultsOnInsert: true };
  // Find the document
  this.session.findOneAndUpdate(query, update, options, function(error, result) {
      if (error) console.log('Error updating the DB' + error);
  });
}

function updateSession(sessionId, sessionName, sessionPublic, sessionActive) {
  var query = {sessionId: sessionId},
      update = {sessionName: sessionName, sessionPublic: sessionPublic, sessionActive: sessionActive},
      options = { new: true, setDefaultsOnInsert: true };
  // Find the document
  this.session.findOneAndUpdate(query, update, options, function(error, result) {
      if (error) console.log('Error updating the DB' + error);
  });
}

module.exports.updateSession = updateSession;
module.exports.createSession = createSession;
module.exports.createItem = createItem;
module.exports.session = session;
module.exports.item = item;
