var models = require('../models.js');
var express = require('express');
var router = express.Router();
const uuidv1 = require('uuid/v1');

/* Authentication related variables */
const okta = require('@okta/okta-sdk-nodejs');
const client = new okta.Client({
  orgUrl: process.env.OKTA_ORG_URL,
  token: process.env.OKTA_TOKEN
});
const fields = [
  { name: 'firstName', label: 'First Name' },
  { name: 'lastName', label: 'Last Name' },
  { name: 'email', label: 'Email', type: 'email' },
  { name: 'password', label: 'Password', type: 'password' }
];

/* GET home page. */
router.get('/', function(req, res, next) {
  // Retrieve all session from the database
  const { userContext } = req;
  if (userContext) {
    models.session.find({sessionowner: userContext.sub}, function(err, sessions) {
      if(err) res.send(err);
      const { userContext } = req;
      res.render('index', { title: 'CollectId', sessions: sessions, userContext: userContext });
    });
  }
  else {
    models.session.find({sessionPublic: 'true', sessionActive: 'true'}, function(err, sessions) {
      if(err) res.send(err);
      res.render('index', { title: 'CollectId', sessions: sessions });
    });
  };
})

/*
app.get('/veggies/explicit-dir', function(req, res) {
  res.render('veggies', {
    title: 'My favorite veggies',
    veggies: veggies,
    layout: relative('views/layout/veggie')
  });
});
*/

/* API */
router.get('/api/getItems', function(req, res, next) {
  // Retrieve all items from the database only if logged userContext
  const { userContext } = req;
  if (userContext) {
    models.item.find(function(err, items) {
      if(err) res.send(err);
      res.json(items);
    });
  };
});

router.get('/api/getItems/:id', function(req, res, next) {
  // Retrieve all items from the database
  models.item.find({sessionId: req.params.id}, function(err, items) {
    if(err) res.send(err);
    res.json(items);
  });
})

/* GET authentication related pages */
router.get('/register', (req, res) => {
  res.render('./authentication/register', { fields })
});

router.post('/register', async (req, res) => {
  const { body } = req

  try {
    await client.createUser({
      profile: {
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        login: body.email
      },
      credentials: {
        password: {
          value: body.password
        }
      }
    })

    res.redirect('/')
  } catch ({ errorCauses }) {
    const errors = {}

    errorCauses.forEach(({ errorSummary }) => {
      const [, field, error] = /^(.+?): (.+)$/.exec(errorSummary)
      errors[field] = error
    })

    res.render('./authentication/register', {
      errors,
      fields: fields.map(field => ({
        ...field,
        error: errors[field.name],
        value: body[field.name]
      }))
    })
  }
});

router.get('/logout', (req, res) => {
  if (req.userContext) {
    const idToken = req.userContext.tokens.id_token;
    const to = encodeURI(process.env.HOST_URL);
    const params = `id_token_hint=${idToken}&post_logout_redirect_uri=${to}`;
    req.logout();
    res.redirect(`${process.env.OKTA_ORG_URL}/oauth2/default/v1/logout?${params}`);
  } else {
    res.redirect('/');
  }
});

/* GET Administration page */
router.get('/administration', function(req, res, next) {
  // if the session ID is 0, then create a new session
  const { userContext } = req;
  if (userContext) {
    switch(req.params.action) {
      default:
        models.session.find(function(err, sessions) {
          if(err) res.send(err);
          models.item.find(function(er, items) {
            if(er) res.send(er);
            res.render('administration', { title: 'CollectId', sessions: sessions, items: items, userContext: userContext });
          });
        });
      };
  } else {
    res.flashMessenger.danger('You need to be logged to access this section');
    res.redirect('/');
  }
});

router.get('/administration/:action', function(req, res, next) {
  // if the session ID is 0, then create a new session
  const { userContext } = req;
  if (userContext) {
    switch(req.params.action) {
      case 'deleteSessions':
        models.session.deleteMany({}, function (err) {
          if (err) res.send(err);
          res.flashMessenger.success('Sessions deleted');
          res.redirect('/administration');
        });
        break;
      case 'deleteItems':
        models.item.deleteMany({}, function (err) {
          if (err) res.send(err);
          res.flashMessenger.success('Items deleted');
          res.redirect('/administration');
        });
        break;
      default:
        models.session.find(function(err, sessions) {
          if(err) res.send(err);
          models.item.find(function(er, items) {
            if(er) res.send(er);
            res.render('administration', { title: 'CollectId', sessions: sessions, items: items, userContext: userContext });
          });
        });
    };
  } else {
    res.flashMessenger.danger('You need to be logged to access this section');
    res.redirect('/');
  };
});

/* GET Board page */
router.get('/board/:id', function(req, res, next) {
  // if the session ID is 0, then create a new session
  if (req.params.id == 0) next('route');
  // otherwise pass the control to the next middleware function in this stack
  else next(); //
  }, function (req, res, next) {
    models.session.findOne({sessionId: req.params.id},'sessionName', function(err, session) {
      if(err) res.send(err);
      res.render('board', { title: 'CollectId', sessionId: req.params.id, sessionName: session.sessionName });
    });
});

/* GET session page */
router.get('/session/:id', function(req, res, next) {
  // if the session ID is 0, then create a new session
  if (req.params.id == 0) next('route');
  // otherwise pass the control to the next middleware function in this stack
  else next(); //
  }, function (req, res, next) {
    // render a regular page starting for the qrcode
    models.session.findOne({sessionId: req.params.id}, function(err, session) {
      if(err) res.send(err);
      const { userContext } = req;
      if (userContext) {
        if (session.sessionOwner.sub == userContext.userinfo.sub)
          res.render('session', { title: 'CollectId', session: session, userContext: userContext  });
      }
      res.render('session', { title: 'CollectId', sessionId: session.sessionId, userContext: userContext  });
    });
});

// handler for the /session/:0 path, which creates a new session
router.get('/session/:id', function (req, res, next) {
  const { userContext } = req;
  if (userContext) {
    var sessionId = uuidv1();
    res.redirect('/createsession/' + sessionId);
  } else {
    res.flashMessenger.danger('You need to be logged to access this section');
    res.redirect('/');
  };
});

// handler for session update
router.post('/session/:id', function(req,res) {
  const { userContext } = req;
  if (userContext) {
    var sessionActive = false;
    var sessionPublic = false;
    if (req.body.sessionPublic == 'on') sessionPublic = true;
    if (req.body.sessionActive == 'on') sessionActive = true;
    models.updateSession(req.params.id, req.body.sessionName, sessionPublic, sessionActive);
    res.flashMessenger.success('Session successfully updated');
    res.redirect('/session/' + req.params.id);
  } else {
      res.redirect('/');
  }
});

/* GET createsession page */
router.get('/createsession/:id', function(req, res) {
    // render the form page to enter session information
    const { userContext } = req;
    res.render('createsession', { title: 'CollectId', sessionId: req.params.id, userContext: userContext });
});

router.post('/createsession/:id', function(req,res) {
  const { userContext } = req;
  if (userContext) {
    var sessionActive = false;
    var sessionPublic = false;
    if (req.body.sessionPublic == 'on') sessionPublic = true;
    if (req.body.sessionActive == 'on') sessionActive = true;
    models.createSession(req.params.id, req.body.sessionName, sessionPublic, sessionActive, {sub: userContext.userinfo.sub, name: userContext.userinfo.name});
    res.flashMessenger.success('Session successfully created');
    res.redirect('/session/' + req.params.id);
  }
});

router.get('/sessions', function(req, res, next) {
  // Retrieve all session from the database
  models.session.find({sessionPublic: 'true'}, function(err, sessions) {
    if(err) res.send(err);
    const { userContext } = req;
    if (userContext) {
      res.render('sessions', { title: 'CollectId', sessions: sessions, userContext: userContext });
    } else {
      res.render('sessions', { title: 'CollectId', sessions: sessions });
    }
  });
});

module.exports = router;
