var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var sessionSchema = new Schema({
  sessionName: String,
  sessionId: String,
  sessionPublic: Boolean,
  sessionActive: Boolean,
  sessionOwner: {
      sub: String,
      name: String
  }
});

module.exports.sessionSchema = sessionSchema;
