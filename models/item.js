var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var itemSchema = new Schema({
  sessionId: String,
  itemTitle: String,
  itemData: String,
  itemOwner: {
    sub: String,
    name: String
  }
});

module.exports.itemSchema = itemSchema;
