var socket = io.connect(window.location.origin);
socket.on('message', function(message) {
  switch(message.content) {
    case 'updateItems':
      // New item created, checking the sessionId to trigger the refresh
      if(message.sessionId == $('#sessionId').val()) {
        getItems(message.sessionId);
      }
      break;
    default:
      console.log('ERROR : action not correct');
    }
})

function getItems(sessionId) {
  var request = new XMLHttpRequest();
  request.open('GET', '/api/getItems/' + sessionId, true);
  request.onload = function () {
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
      $('#itemsContainer').html('');
      data.forEach(itm => {
        $('#itemsContainer').append("<li><div class=\"tile\"><a><h2>" + itm.itemTitle + "</h2><p>" + itm.itemData + "</p></a></div></li>");
      });
    } else {
      console.log('error');
    }
  }
  request.send();
}

$('#poke').click(function () {
    var msg = new Object();
    msg.action = "sendItem";
    msg.itemData = $('#Data').val();
    msg.itemTitle = $('#Title').val();
    msg.sessionId = $('#sessionId').val();
    msg.itemOwner = {};
    socket.emit('message', msg);
});

$('#boke').click(function () {
  getItems($('#sessionId').val());
});
