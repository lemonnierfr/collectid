Feature: Post page

  Background:
    Given I browse "http://localhost:8080/"

  Scenario: See the homepage
    Given I am on homepage
    Then I should be on "/"
    And I should see "CollectId"

  Scenario: Accessing Admin page anonymously
    Given I am on homepage
    When I am on "/administration"
    Then I should be on "/"

  Scenario: Creating session anonymously
    Given I am on homepage
    When I am on "/session/0"
    Then I should be on "/"

  Scenario: Logging in
    Given I am on homepage
    And I follow "log in"
    And I fill in the following:
      | input[id='okta-signin-username']   | test@test.com |
      | input[id='okta-signin-password']   | collectId0    |
    When I submit "form#form17" form
    And I take a screenshot
    Then I should be on "/"
    And I should see "Hi test!"

  Scenario: Cleaning sessions
    Given I am on "/administration"
    When I follow "Delete all sessions"
    Then I should be on "/administration"
    And I should not see "session de test"

  Scenario: Cleaning items
    Given I am on "/administration"
    When I follow "Delete all items"
    Then I should be on "/administration"
    And I should not see "item de test"

  Scenario: Create to a new session
    Given I am on "/session/0"
    When I fill in the following:
      | input[name='sessionName']    | session de test |
      | input[name='sessionOwner']   | Owner de test   |
    And I check "#sessionStatus"
    And I submit "form" form
    Then the url should match ^\/session\/*
    And I should see "Please provide this URL to participants"

  Scenario: Adding a new item
    Given I am on "/"
    And I follow "session de test"
    And I click on "a[href='#']"
    And I fill in the following:
      | input[name='Title']    | item de test      |
      | textarea[name='Data']  | contenu de test   |
    And I wait 2 seconds
    And I click on "button[id='poke']"
    And I wait 2 seconds
    And I am on "/sessions"
    When I follow "session de test"
    Then I should see "item de test"
    And I should see "contenu de test"

  Scenario: Accessing an existing board
    Given I am on "/sessions"
    When I follow "session de test"
    Then the url should match ^\/board\/*
    And I should see "session de test"

  Scenario: Adding a new item
    Given I am on "/sessions"
    When I follow "session de test"
    Then the url should match ^\/board\/*
    And I should see "session de test"
