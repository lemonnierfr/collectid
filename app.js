var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
const { ExpressOIDC } = require('@okta/oidc-middleware');
var hbs = require('express-hbs');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var fp = require('path');
var FlashMessenger = require('flash-messenger');

var indexRouter = require('./routes/index');

var app = express();

function relative(path) {
  return fp.join(__dirname, path);
}
var viewsDir = relative('views');

// Authentication engine
const oidc = new ExpressOIDC({
  issuer: `${process.env.OKTA_ORG_URL}/oauth2/default`,
  client_id: process.env.OKTA_CLIENT_ID,
  client_secret: process.env.OKTA_CLIENT_SECRET,
  redirect_uri: `${process.env.HOST_URL}/authorization-code/callback`,
  appBaseUrl: `${process.env.HOST_URL}`,
  scope: 'openid profile'
})

app.use(session({
  secret: process.env.APP_SECRET,
  resave: true,
  saveUninitialized: false
}));

app.use(oidc.router);


// view engine setup
// Hook in express-hbs and tell it where known directories reside
app.engine('hbs', hbs.express4({
  partialsDir: [relative('views/partials'), relative('views/partials-other')],
  layoutsDir: relative('views/layout'),
  defaultLayout: relative('views/layout/default.hbs')
}));
app.set('view engine', 'hbs');
app.set('views', relative('views'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(relative('public')));
app.use(FlashMessenger.middleware);

app.use('/', indexRouter);

// Register sync helper
hbs.registerHelper('link', function(text, options) {
  var attrs = [];
  for (var prop in options.hash) {
    attrs.push(prop + '="' + options.hash[prop] + '"');
  }
  return new hbs.SafeString('<a ' + attrs.join(' ') + '>' + text + '</a>');
});

// Register Async helpers
hbs.registerAsyncHelper('readFile', function(filename, cb) {
  fs.readFile(fp.join(viewsDir, filename), 'utf8', function(err, content) {
    if (err) console.error(err);
    cb(new hbs.SafeString(content));
  });
});

// Debug sync helper
hbs.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', { error: err });
});

module.exports = app;
