#!/usr/bin/env node

/**
 * Module dependencies.
 */
require('dotenv').config();

var app = require('../app');
var debug = require('debug')('collectid:server');
var http = require('http');
var models = require("../models.js");

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
const io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {
  socket.on('message', function (message) {
       executeMessage(message);
   });
 });

server.listen(process.env.PORT);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Messages management function
 */
function executeMessage(msg) {
  console.log('Starting Executing message');
  switch(msg.action) {
    case 'sendItem':
      // New item received, Adding to DB and notifying client
      models.createItem(msg);
      io.sockets.emit('message', { content: 'updateItems', sessionId: msg.sessionId });
      break;
    default:
      console.log('ERROR : action not correct');
      break;
    }
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(process.env.PORT)) {
    // named pipe
    return val;
  }

  if (process.env.PORT >= 0) {
    // port number
    return process.env.PORT;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof process.env.PORT === 'string'
    ? 'Pipe ' + process.env.PORT
    : 'Port ' + process.env.PORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
  console.log('Server successfully started on ' + bind);
}

module.exports.executeMessage = executeMessage;
